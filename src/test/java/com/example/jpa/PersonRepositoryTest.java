package com.example.jpa;

import com.example.jpa.pojo.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class PersonRepositoryTest {


    @Autowired
    @Qualifier("masterPersonService")
    com.example.jpa.master.PersonService masterPersonService;

    @Test
    void o() {
        List<Person> people = masterPersonService.list();
        people.forEach(System.out::println);
    }

    @Test
    void s() {
        List<Person> people = masterPersonService.saveAll();
        people.forEach(System.out::println);
    }


    @Autowired
    @Qualifier("slavePersonService")
    com.example.jpa.slave.PersonService slavePersonService;

    @Test
    void o1() {
        List<Person> people = slavePersonService.list();
        people.forEach(System.out::println);
    }

    @Test
    void s1() {
        List<Person> people = slavePersonService.saveAll();
        people.forEach(System.out::println);
    }


}
