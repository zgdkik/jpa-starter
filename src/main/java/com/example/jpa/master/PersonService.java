package com.example.jpa.master;

import com.example.jpa.annotation.DataSource;
import com.example.jpa.constant.DataSourcesType;
import com.example.jpa.pojo.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 86188
 */
@Service("masterPersonService")
public class PersonService {
    @Autowired
    @Qualifier("masterPersonRepository")
    PersonRepository masterPersonRepository;

    public List<Person> list() {
        return masterPersonRepository.findAll();
    }

    public List<Person> saveAll() {
        List<Person> list = new ArrayList();
        Person person = new Person("李白", 23, "武汉");
        Person person1 = new Person("李四", 24, "上海");
        Person person2 = new Person("王五", 25, "深圳");
        Person person3 = new Person("赵六", 26, "广州");
        Person person4 = new Person("约翰", 27, "北京");
        list.add(person);
        list.add(person1);
        list.add(person2);
        list.add(person3);
        list.add(person4);

        LocalDateTime now = LocalDateTime.now();
        for (Person p : list) {
            p.setCreateTime(now);
            p.setCreateUser("OS");
        }

        List<Person> people = masterPersonRepository.saveAll(list);
        return people;
    }
}
