package com.example.jpa.annotation;

import com.example.jpa.constant.DataSourcesType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 86188
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited//表示该注解可以被子类继承,注意,仅针对类,成员属性、方法并不受此注释的影响
public @interface DataSource {
    DataSourcesType name() default DataSourcesType.MASTER;
}
