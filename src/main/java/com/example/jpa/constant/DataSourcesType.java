package com.example.jpa.constant;

/**
 * 数据源类型
 * @author 86188
 */
public enum DataSourcesType {
    MASTER,
    SLAVE
}
